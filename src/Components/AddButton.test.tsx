require('../setupTests');
import * as React from 'react';
import { shallow } from 'enzyme';
import AddButton from './AddButton';
import { Provider } from 'react-redux';
import store from '../store';

describe('AddButton Component', () => {
	it('Should match the snapshot', () => {
		const wrapper = shallow(
			<Provider store={store}>
				<AddButton />
			</Provider>
		);

		expect(wrapper).toMatchSnapshot();
	});

	it('Should have a click function', () => {
		const wrapper = shallow(
			<Provider store={store}>
				<AddButton />
			</Provider>
		);

		wrapper.simulate('click');
	});
});
