require('../setupTests');
import * as React from 'react';
import { shallow } from 'enzyme';
import Header from './Header';

describe('Header component', () => {
	it('it should render without errors', () => {
		const wrapper = shallow(<Header />);
		wrapper.setProps({ children: <div /> });
		expect(wrapper.props().children).toEqual(<div />);
	});

	it('Should match the snapshot', () => {
		const wrapper = shallow(<Header />);
		expect(wrapper).toMatchSnapshot();
	});
});
