import styled from 'styled-components';

interface ContainerProps {
	readonly dark?: boolean;
}

export default styled.div<ContainerProps>`
	background-color: ${(props) => (props.dark ? '#F0F0F0' : '#FFF')};
	padding: 15px;
	display: flex;
	justify-content: space-between;
`;