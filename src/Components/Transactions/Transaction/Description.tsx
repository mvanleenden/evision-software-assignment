import styled from 'styled-components';
import { textColor } from '../../../Constants/Colors';

export default styled.p`
	color: ${textColor};
`;
