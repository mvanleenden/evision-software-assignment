require('../../../setupTests');
import * as React from 'react';
import { shallow, mount } from 'enzyme';
import Transaction from './index';
import { transactionType } from '../../../Constants/Enums';

describe('Transaction Component', () => {
	it('Should match the snapshot', () => {
		const props = {
			type: transactionType.CREDIT,
			name: '',
			description: '',
			date: '',
			amount: 234,
			index: 0
    };
    
    const wrapper = shallow(<Transaction {...props} />);
    expect(wrapper).toMatchSnapshot()
  });
  
  it('Should match the props', () => {
    const props = {
			type: transactionType.CREDIT,
			name: '',
			description: '',
			date: '',
			amount: 234,
			index: 0
    };
    
    const wrapper = mount(<Transaction {...props} />);
    expect(wrapper.prop('amount')).toEqual(234)

  })
});
