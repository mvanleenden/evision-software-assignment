import * as React from 'react';
import { transactionType } from '../../../Constants/Enums';

import Amount from './Amount';
import Container from './Container';
import Description from './Description';
import Name from './Name';
import Time from './Time';

export interface ITransaction {
	type: transactionType;
	name: string | undefined;
	description: string;
	amount: number;
	date: string;
	index: number;
}

const Transaction: React.FC<ITransaction> = ({ type, name, description, amount, date, index }) => {
	return (
		<Container dark={index % 2 === 0}>
			<div>
				<Name>{name}</Name>
				<Description>{description}</Description>
			</div>
			<div>
				<Amount type={type} amount={amount} />
				<Time>{date}</Time>
			</div>
		</Container>
	);
};

export default Transaction;
