import * as React from 'react';
import styled from 'styled-components';
import { transactionType } from '../../../Constants/Enums';

interface IContainerProps {
	transactionType: transactionType;
}

const Container = styled.h3 <IContainerProps>`
	margin-bottom: 5px;
	text-align: right;
	color: ${(props) => (props.transactionType === transactionType.CREDIT ? 'red' : 'green')};
`;

interface IAmount {
	type: transactionType;
	amount: number;
}

const Amount: React.FC<IAmount> = ({ type, amount }) => {
	const formattedAmount = Number.parseFloat(`${amount}`).toFixed(2)
	return (
		<Container transactionType={type}>{type === transactionType.CREDIT ? `- ` : `+ `} {formattedAmount}</Container>
	);
};

export default Amount;
