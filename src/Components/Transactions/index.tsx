import * as React from 'react';
import styled from 'styled-components';
import Transaction from './Transaction/index';
import { ITransaction } from '../../Constants/Interfaces';

interface ITransactions {
	list: ITransaction[];
}

const ScrollContainer = styled.section`
	overflow-y: scroll;
	height: 68vh;
	box-sizing: content-box;
`;

const Transactions: React.FC<ITransactions> = ({ list }) => {
	return (
		<ScrollContainer>
			{list && list.map((transaction, index) => <Transaction key={index} {...transaction} index={index} />)}
		</ScrollContainer>
	);
};

export default Transactions;
