import styled from 'styled-components';
import { primaryColor } from '../Constants/Colors';

export default styled.header`
	height: 20vh;
	background-color: ${primaryColor};
	border-radius: 0 0 30px 0;
	display: flex;
	align-items: center;

	@media screen and (max-width: 1024px){
		height: 20vh
	}

`;
