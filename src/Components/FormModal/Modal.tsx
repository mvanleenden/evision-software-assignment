import * as React from 'react';
import { Button, Modal, Form, Message } from 'semantic-ui-react';
import { transactionType, status } from '../../Constants/Enums';

const options: { text: string; value: transactionType }[] = [
	{
		text: 'Send Money',
		value: transactionType.CREDIT
	},
	{
		text: 'Receive Money',
		value: transactionType.DEBIT
	}
];

interface IValues {
	type: transactionType;
	name: string;
	amount: string;
	description: string;
}

interface ModalProps {
	values: IValues;
	isValid: boolean;
	formStatus: status;
	onChange: (name: string, value: any) => void;
	toggleModal: () => void;
	onSubmit: () => void;
}

const ModalForm: React.FC<ModalProps> = ({ onChange, values, toggleModal, onSubmit, isValid, formStatus }) => {
	const isOpen: boolean = formStatus !== status.CLOSED && formStatus !== status.SUCCESS;
	const isLoading = formStatus === status.LOADING;

	return (
		<Modal open={isOpen} onClose={toggleModal}>
			<Modal.Header>New Transaction</Modal.Header>

			{formStatus === status.FAILURE && (
				<Message warning header="Failed!" content="Something went wrong!, please try again" />
			)}

			<Modal.Content>
				<Form loading={isLoading}>
					<Form.Select
						defaultValue={values.type}
						onChange={(_, val) => onChange('type', val.value)}
						options={options}
						placeholder="Transaction Type"
						label={'Transaction Type:'}
					/>
					<Form.Input
						fluid
						label={values.type === 0 ? 'From:' : 'To:'}
						name="name"
						value={values.name}
						onChange={(event) => onChange(event.target.name, event.target.value)}
					/>
					<Form.Input
						type="number"
						fluid
						label="Amount"
						name="amount"
						value={values.amount}
						onChange={(event) => onChange(event.target.name, event.target.value)}
					/>
					<Form.TextArea
						value={values.description}
						label="Description"
						name="description"
						onChange={(_, val) => onChange('description', val.value)}
					/>

					<Button disabled={!isValid} onClick={onSubmit}>
						Add
					</Button>
				</Form>
			</Modal.Content>
		</Modal>
	);
};

export default ModalForm;
