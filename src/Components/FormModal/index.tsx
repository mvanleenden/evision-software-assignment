import * as React from 'react';
import Modal from './Modal';
import { connect } from 'react-redux';
import { transactionType, status } from '../../Constants/Enums';
import { addTransaction } from '../../actions/data';
import { changeFormStatus } from '../../actions/status';

const defaultState = {
	values: {
		type: transactionType.DEBIT,
		name: '',
		amount: '',
		description: ''
	},
	isValid: false
};

class AddForm extends React.Component<any> {
	state = { values: { ...defaultState.values }, isValid: defaultState.isValid };

	onChange = (name: string, value: any) => {
		const isValid: boolean = this.isFormValid({ ...this.state.values, [name]: value });
		this.setState({ values: { ...this.state.values, [name]: value }, isValid });
	};

	componentWillReceiveProps(props: any) {
		if (props.status.form === status.SUCCESS) {
			this.setState({ values: { ...defaultState.values }, isValid: defaultState.isValid });
		}
	}

	onSubmit = () => {
		const { name, amount, description, type } = this.state.values;
		this.props.addTransaction(type, name, amount, description);
	};

	isFormValid = (values: any): boolean => {
		let isValid: boolean = true;

		if (!values.name || values.name.length <= 1) {
			isValid = false;
		} else if (!values.amount || Number.parseFloat(values.amount) <= 0) {
			isValid = false;
		}

		return isValid;
	};

	render() {
		return (
			<Modal
				formStatus={this.props.status.form}
				values={this.state.values}
				onChange={this.onChange}
				onSubmit={this.onSubmit}
				toggleModal={() => this.props.changeFormStatus(status.CLOSED)}
				isValid={this.state.isValid}
			/>
		);
	}
}

const mapStateToProps = (state: any) => {
	return {
		status: state.status
	};
};

export default connect(mapStateToProps, { changeFormStatus, addTransaction })(AddForm);
