require('../../setupTests');
import * as React from 'react';
import { shallow, mount } from 'enzyme';
import AccountInfo from './index';

describe('AccountInfo Component', () => {
  it('Should match snapshot', () => {
    const props = {
      name: '',
      iban: '',
      balance: 0,
      currency: ''
    }
    const wrapper = shallow(<AccountInfo {...props}/>)

    expect(wrapper).toMatchSnapshot()
  })

  it('Should match the props', () => {

    const props = {
      name: 'Mayron van Leenden',
      iban: '',
      balance: 100,
      currency: ''
    }
    const wrapper = mount(<AccountInfo {...props}/>)

    expect(wrapper.prop('name')).toEqual('Mayron van Leenden')
    expect(wrapper.prop('balance')).toEqual(100)

    wrapper.setProps({balance: 250})
    expect(wrapper.prop('balance')).toEqual(250)
  })
});
