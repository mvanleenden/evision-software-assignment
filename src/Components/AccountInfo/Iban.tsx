import styled from 'styled-components';
import { lightGray } from '../../Constants/Colors';

export default styled.h2`
	color: ${lightGray};
	margin: 0;
	padding: 0;
	font-size: 1rem;
`;