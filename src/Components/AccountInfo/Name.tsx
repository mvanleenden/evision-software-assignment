import styled from 'styled-components';

export default styled.h1`
	color: #fff;
	font-size: 1.5rem;
	margin: 0;
	padding: 0;
`;
