import * as React from 'react';
import styled from 'styled-components';
import { lightGray } from '../../Constants/Colors';

interface IBalance {
	amount: number;
	currency: string;
}

const Container = styled.div`text-align: right;`;

const Title = styled.h1`
	margin: 0;
	padding: 0;
	color: ${lightGray};
	font-size: 1rem;
`;

const Amount = styled.h2`
	margin: 0;
	padding: 0;
	color: #fff;
	font-size: 1.5rem;
`;

const Balance: React.FC<IBalance> = ({ amount, currency }) => {
	return (
		<Container>
			<Title>BALANCE</Title>
			<Amount>{`${currency} ${amount.toFixed(2)}`}</Amount>
		</Container>
	);
};

export default Balance;
