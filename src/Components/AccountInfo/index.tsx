import * as React from 'react';
import styled from 'styled-components';

import Balance from './Balance';
import Iban from './Iban';
import Name from './Name';

const Container = styled.section`
	display: grid;
	grid-template-columns: 1fr 1fr;
`;
export interface IAccountProps {
	name: string;
	iban: string;
	balance: number;
	currency: string;
}

const AccountInfoContainer: React.FC<IAccountProps> = (props) => {
	return (
		<Container>
			<div>
				<Name>{props.name}</Name>
				<Iban>IBAN: {props.iban}</Iban>
			</div>
			<Balance currency={props.currency} amount={props.balance} />
		</Container>
	);
};

export default AccountInfoContainer;
