import * as React from 'react';
import styled from 'styled-components';
import Header from './Header';
import Content from './Content';
import AccountInfo from './AccountInfo';
import TopBar from './TopBar';
import Transactions from './Transactions';
import { IAccount, ITransaction } from '../Constants/Interfaces';
import Form from './FormModal';

interface IDashboard {
	account: IAccount;
	currency: string;
	transactions: ITransaction[];
}

const Body = styled.main`height: 80vh;`;

const Dashboard: React.FC<IDashboard> = ({ account, currency, transactions }) => {
	return (
		<React.Fragment>
			<Header>
				<Content>
					<AccountInfo
						name={account.name}
						iban={account.iban}
						balance={account.balance}
						currency={currency}
					/>
				</Content>
			</Header>
			<Body>
				<Content>
					<TopBar />
					{transactions && <Transactions list={transactions} />}
				</Content>
			</Body>
			<Form/>
		</React.Fragment>
	);
};

export default Dashboard;
