import * as React from 'react';
import styled from 'styled-components';
import { textColor } from '../Constants/Colors';
import AddButton from './AddButton';

const Container = styled.section`
	display: flex;
	align-items: center;
	justify-content: space-between;
	padding: 15px 0;
`;

const Title = styled.h1`
	color: ${textColor};
	text-transform: uppercase;
	font-size: 1.3rem;
	padding: 0;
	margin: 0;
`;

const TopBar: React.FC = () => {
	return (
		<Container>
			<Title>Transactions</Title>
			<AddButton/>
		</Container>
	);
};

export default TopBar;
