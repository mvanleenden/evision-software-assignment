import styled from 'styled-components';

export default styled.section`
	width: 65%;
	margin: 0 auto;
	box-sizing: border-box;

	@media screen and (max-width: 1024px){
		width: 85%
	}

	@media screen and (max-width: 736px) {
		width: 95%;
	}
`;
