import React from 'react'
import { Loader } from 'semantic-ui-react'
import styled from 'styled-components';

const Container = styled.div`
 height: 100%;
 display: flex;
 align-items: 'center';
 justify-content: 'center';
`

export default () => (
  <Container>
    <Loader active />
  </Container>
)
