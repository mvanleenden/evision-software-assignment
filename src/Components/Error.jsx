import * as React from 'react';
import styled from 'styled-components';
import { FaRedo } from 'react-icons/fa'

const Container = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;
`

export default () => (
  <Container>
      <h2>Oh no! something went wrong</h2>
      <h3>Please refresh <FaRedo onClick={() => window.location.reload()} /> your page</h3>
  </Container>
)
