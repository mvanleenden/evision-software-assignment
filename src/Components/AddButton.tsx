import * as React from 'react';
import styled from 'styled-components';
import { FaPlus } from 'react-icons/fa';
import { connect } from 'react-redux';
import { primaryColor } from '../Constants/Colors';
import { changeFormStatus } from '../actions/status'
import { status } from '../Constants/Enums';

const Container = styled.button`
	height: 60px;
	width: 60px;
	background-color: ${primaryColor};
	border-radius: 30px;
	display: flex;
	align-items: center;
	justify-content: center;

	@media screen and (max-width: 736px) {
		width: 50px;
		height: 50px;
		border-radius: 25px;
	}
`;

const AddButton: React.FC<any> = ({changeFormStatus}) => {
	return (
		<Container onClick={() => changeFormStatus(status.OPEN)} >
			<FaPlus size={18} color={'#FFF'} />
		</Container>
	);
};

export default connect(null, { changeFormStatus })(AddButton);
