import { status } from '../Constants/Enums';

export const CHANGE_DATA_STATUS = 'CHANGE_DATA_STATUS';
export const changeDataStatus = (status: status) => {
	return {
		type: CHANGE_DATA_STATUS,
		payload: status
	};
};

export const CHANGE_FORM_STATUS = 'CHANGE_FORM_STATUS';
export const changeFormStatus = (status: status) => {
	return {
		type: CHANGE_FORM_STATUS,
		payload: status
	};
};
