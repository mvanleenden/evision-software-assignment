import axios from 'axios';
import { transactionType, currency, status } from '../Constants/Enums';
import { ITransaction, IAccount, IDebitAndCredit } from '../Constants/Interfaces';
import { changeDataStatus, changeFormStatus } from './status';
import dataConverter from '../utils/dataConverter';

interface IData {
	transactions: ITransaction[];
	account: IAccount;
	currency: string;
}

export const LOAD_DATA = 'LOAD_DATA';
const onDataLoad = (data: IData) => {
	return {
		type: LOAD_DATA,
		payload: data
	};
};

export const loadData = () => (dispatch: any) => {
	axios
		.get('http://localhost:8080/api/balance')
		.then(async ({ data }) => {
			const transactions: ITransaction[] = await dataConverter(data.debitsAndCredits);
			dispatch(onDataLoad({ transactions, account: data.account, currency: currency[data.currency] }));
			dispatch(changeDataStatus(status.SUCCESS));
		})
		.catch((err) => {
			dispatch(changeDataStatus(status.FAILURE));
		});
};

export const ADD_TRANSACTION = 'ADD_TRANSACTION';
const onTransactionAdded = (transaction: ITransaction) => {
	return {
		type: ADD_TRANSACTION,
		payload: transaction
	};
};

export const addTransaction = (type: number, name: string, amount: number, description: string) => (dispatch: any) => {
	dispatch(changeFormStatus(status.LOADING));

	const fromOrTo: string = type === transactionType.CREDIT ? 'to' : 'from';
	const date: string = new Date().toISOString();

	const data: IDebitAndCredit = {
		[fromOrTo]: name,
		date,
		amount: Number.parseFloat(amount.toString()),
		description
	};

	axios
		.put('http://localhost:8080/api/balance/add', JSON.stringify(data) )
		.then(async () => {
			const transformedData: ITransaction[] = await dataConverter([ data ]);

			dispatch(onTransactionAdded(transformedData[0]));
			dispatch(changeFormStatus(status.SUCCESS));
		})
		.catch((err) => {
			console.log(err)
			dispatch(changeFormStatus(status.FAILURE));
		});
};
