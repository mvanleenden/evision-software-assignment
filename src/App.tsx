import React from 'react';
import Dashboard from './Components/Dashboard';
import { connect } from 'react-redux';
import { loadData } from './actions/data';
import { status } from './Constants/Enums';
import Loader from './Components/Loader';
import Error from './Components/Error'

class App extends React.Component<any> {

	componentDidMount() {
		this.props.loadData();
	}

	render() {
		const { currency, account, transactions } = this.props.data
		return (
			<React.Fragment>
				{this.props.status.data === status.SUCCESS && <Dashboard account={account} currency={currency} transactions={transactions}/>}
				{this.props.status.data === status.LOADING &&  <Loader />}	
				{this.props.status.data === status.FAILURE && <Error/>}
			</React.Fragment>
		);
	}
}

const mapStateToProps = (state: any) => {
	return {
		data: state.data,
		status: state.status
	};
};

export default connect(mapStateToProps, { loadData })(App);
