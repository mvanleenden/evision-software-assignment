import { IDebitAndCredit, ITransaction } from '../Constants/Interfaces';
import { transactionType } from '../Constants/Enums';

const dataConverter = async (debitsAndCredits: IDebitAndCredit[]): Promise<ITransaction[]> => {
	return new Promise((resolve, reject) => {
		const transformed: ITransaction[] = debitsAndCredits
			.map((item) => {
				return {
					type: item.from ? transactionType.DEBIT : transactionType.CREDIT,
					name: item.from ? item.from : item.to,
					date: new Date(item.date).toDateString(),
					amount: item.amount,
					description: item.description
				};
			})
			.sort((a, b) => {
				return new Date(b.date).getTime() > new Date(a.date).getTime() ? 1 : -1;
			});

		resolve(transformed);
	});
};

export default dataConverter;
