import { ITransaction, IAccount } from '../Constants/Interfaces';
import { LOAD_DATA, ADD_TRANSACTION } from '../actions/data'
import { transactionType } from '../Constants/Enums';

interface IState {
	transactions: ITransaction[];
	account: IAccount | null;
	currency?: string | null;
}

const initialState: IState = {
	transactions: [],
	account: null,
	currency: null
};

export default (state: IState = initialState, action: any = {}) => {
	switch (action.type) {
		case LOAD_DATA:
			return { transactions: [...action.payload.transactions], account: {...action.payload.account}, currency: action.payload.currency}
		case ADD_TRANSACTION:
			let newBalance:number = 0;

			if(state.account) {
				newBalance = state.account.balance
			}

			if(action.payload.type === transactionType.DEBIT) {
				newBalance += action.payload.amount 
			} else {
				newBalance -= action.payload.amount
			}

			let accountInfo:IAccount | null = state.account
			if(accountInfo) {
				accountInfo = {...accountInfo, balance: newBalance}
			}

			return {...state, transactions: [{...action.payload}, ...state.transactions], account: accountInfo}
		default:
			return state;
	}
};
