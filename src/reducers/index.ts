import { combineReducers } from 'redux';
import data from './data';
import status from './status';

export default combineReducers({
  data,
  status
});
