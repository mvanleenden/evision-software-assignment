import { status } from '../Constants/Enums';
import { CHANGE_DATA_STATUS, CHANGE_FORM_STATUS } from '../actions/status';

interface IState {
	form: status;
	data: status;
}

const initialState: IState = {
	form: status.CLOSED,
	data: status.LOADING
};

export default (state = initialState, action: any = {}) => {
	switch (action.type) {
		case CHANGE_DATA_STATUS:
			return { ...state, data: action.payload };
		case CHANGE_FORM_STATUS:
			return { ...state, form: action.payload };
		default:
			return state;
	}
};
