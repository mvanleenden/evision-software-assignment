export enum currency {
	EURO = '€',
	USD = '$'
}

export enum transactionType {
	DEBIT,
	CREDIT
}

export enum status {
	LOADING,
	SUCCESS,
	FAILURE,
	OPEN,
	CLOSED
}
