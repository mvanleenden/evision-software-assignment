import { transactionType } from './Enums';

export interface ITransaction {
	name: string | undefined;
	type: transactionType;
	description: string;
	amount: number;
	date: string;
}

export interface IAccount {
	name: string;
	iban: string;
	balance: number;
}

export interface IForm {
	type: transactionType;
	name: string;
	amount: number;
	description: string;
}

export interface IDebitAndCredit {
	from?: string;
	to?: string;
	description: string;
	amount: number;
	date: string;
}
