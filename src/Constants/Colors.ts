export const primaryColor: string = '#1d1e22';
export const lightGray: string = '#CBC7C7';
export const textColor: string = '#555555';
