
import { createStore, applyMiddleware, compose } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducer from './reducers';

const enhancer = compose(applyMiddleware(ReduxThunk));

export default createStore(reducer, enhancer);