## Evision Software Assignment
Build a simple web app that can display the balance result from the provided api server.

### Requirements
* The app should take up all screen height
* The content should be horizontally centered
* The list of credits and debits should be scrollable
* The app should work in all major browser (latest version)

### Final Project
![Image of the test](./sample.png)


## Built With 👇
* React
* TypeScript
* [Styled-Components](https://www.npmjs.com/package/styled-components)
* [Semantic-UI](https://www.npmjs.com/package/semantic-ui-react): Used on the following components: [Request Form](./src/Components/FormModal/Modal.tsx) & [Loader](./src/Components/Loader.tsx) 
* [Redux](https://www.npmjs.com/package/redux)
* [Redux Thunk](https://www.npmjs.com/package/redux-thunk)
* [Concurrently](https://www.npmjs.com/package/concurrently)
* [Axios](https://www.npmjs.com/package/axios)
* [React Icons](https://www.npmjs.com/package/react-icons)


## Installing

### Clone the project
```
git clone git@bitbucket.org:mvanleenden/evision-software-assignment.git 
```

### Dependencies
```
npm install
```

### Start Script (include the server)
```
npm start
```


## Running the tests
```
npm test
```

### I've implemented test for the following components:
* [App](./src/App.test.tsx)
* [Header](./src/Components/Header.test.tsx)
* [Button](./src/Components/AddButton.test.tsx)
* [Transaction](./src/Components/Transactions/Transaction/Transaction.test.tsx)
